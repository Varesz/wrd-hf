package com.example.pollbackend.service;

import com.example.pollbackend.DTOs.AnswerDTO;
import com.example.pollbackend.DTOs.AnswerStatisticsDTO;
import com.example.pollbackend.DTOs.QuestionDTO;
import com.example.pollbackend.DTOs.QuestionStatisticsDTO;
import com.example.pollbackend.model.Answer;
import com.example.pollbackend.model.Poll;
import com.example.pollbackend.model.Question;
import com.example.pollbackend.model.QuestionType;
import com.example.pollbackend.repository.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PollService {

    private final PollRepository pollRepository;

    @Autowired
    public PollService(PollRepository pollRepository) {
        this.pollRepository = pollRepository;
    }

    public List<QuestionDTO> getQuestionnaire() {
        List<Poll> polls = pollRepository.findAll();
        List<Question> questions = convertToQuestions(polls);
        return questions.stream()
                .map(this::convertToQuestionDTO)
                .collect(Collectors.toList());
    }

    private List<Question> convertToQuestions(List<Poll> polls) {
        List<Question> questions = new ArrayList<>();
        for (Poll poll : polls) {
            questions.addAll(poll.getQuestions());
        }
        return questions;
    }

    public void submitAnswers(List<AnswerDTO> answerDTOs) {
        for (AnswerDTO answerDTO : answerDTOs) {
            Long questionId = answerDTO.getQuestionId();

            Poll poll = pollRepository.findById(questionId)
                    .orElseThrow(() -> new RuntimeException("Poll not found"));

            Question question = poll.getQuestions().stream()
                    .filter(q -> q.getId().equals(questionId))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Question not found"));

            if (question.getType() == QuestionType.SELECT_ONE || question.getType() == QuestionType.SELECT_MORE) {
                List<Answer> selectedAnswers = answerDTO.getSelectedAnswers().stream()
                        .map(answerText -> new Answer(answerText, question))
                        .collect(Collectors.toList());
                question.setAnswers(selectedAnswers);
            } else if (question.getType() == QuestionType.FREE_TEXT) {
                Answer freeTextAnswer = new Answer(answerDTO.getFreeTextAnswer(), question);
                question.setAnswers(List.of(freeTextAnswer));
            }

            pollRepository.save(poll);
        }
    }

    public QuestionStatisticsDTO getAnswerStatistics(Long questionId) {
        Poll poll = pollRepository.findById(questionId)
                .orElseThrow(() -> new RuntimeException("Poll not found"));

        List<Question> questions = poll.getQuestions();

        Question question = questions.get(Math.toIntExact(questionId));

        List<Answer> answers = question.getAnswers();

        List<AnswerStatisticsDTO> answerStatistics = answers.stream()
                .collect(Collectors.groupingBy(Answer::getText, Collectors.counting()))
                .entrySet()
                .stream()
                .map(entry -> new AnswerStatisticsDTO(entry.getKey(), entry.getValue()))
                .sorted(Comparator.comparingLong(AnswerStatisticsDTO::getCount).reversed())
                .collect(Collectors.toList());

        QuestionStatisticsDTO questionStatisticsDTO = new QuestionStatisticsDTO();
        questionStatisticsDTO.setQuestion(question.getQuestion());
        questionStatisticsDTO.setType(question.getType());
        questionStatisticsDTO.setAnswers(answerStatistics);

        return questionStatisticsDTO;
    }

    private QuestionDTO convertToQuestionDTO(Question question) {
        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setQuestion(question.getQuestion());
        questionDTO.setType(question.getType());
        if (question.getAnswers() != null) {
            questionDTO.setAnswers(question.getAnswers().stream().map(Answer::getText).collect(Collectors.toList()));
        }
        return questionDTO;
    }
}