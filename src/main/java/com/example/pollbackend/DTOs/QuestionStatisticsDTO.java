package com.example.pollbackend.DTOs;

import com.example.pollbackend.model.QuestionType;
import lombok.Setter;

import java.util.List;

@Setter
public class QuestionStatisticsDTO {
    private String question;
    private QuestionType type;
    private List<AnswerStatisticsDTO> answers;
}