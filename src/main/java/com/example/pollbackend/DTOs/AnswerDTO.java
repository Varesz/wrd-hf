package com.example.pollbackend.DTOs;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class AnswerDTO {
    private Long questionId;
    private List<String> selectedAnswers;
    private String freeTextAnswer;
}