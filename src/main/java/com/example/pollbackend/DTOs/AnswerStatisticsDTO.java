package com.example.pollbackend.DTOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerStatisticsDTO {
    private String text;
    private Long count;


    public AnswerStatisticsDTO(String answerText, Long count) {
        this.text = answerText;
        this.count = count;
    }
}