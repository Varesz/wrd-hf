package com.example.pollbackend.repository;

import com.example.pollbackend.model.Poll;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollRepository extends JpaRepository<Poll, Long> { }
