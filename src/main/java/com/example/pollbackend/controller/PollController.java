package com.example.pollbackend.controller;

import com.example.pollbackend.DTOs.AnswerDTO;
import com.example.pollbackend.DTOs.QuestionDTO;
import com.example.pollbackend.DTOs.QuestionStatisticsDTO;
import com.example.pollbackend.service.PollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PollController {

    private final PollService pollService;

    @Autowired
    public PollController(PollService pollService) {
        this.pollService = pollService;
    }

    @GetMapping("/questionnaire")
    public ResponseEntity<List<QuestionDTO>> getQuestionnaire() {
        List<QuestionDTO> questions = pollService.getQuestionnaire();
        return ResponseEntity.ok(questions);
    }

    @PostMapping("/answers")
    public ResponseEntity<Void> submitAnswers(@RequestBody List<AnswerDTO> answerDTOs) {
        pollService.submitAnswers(answerDTOs);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/answer-statistics/{questionId}")
    public ResponseEntity<QuestionStatisticsDTO> getAnswerStatistics(@PathVariable Long questionId) {
        QuestionStatisticsDTO statistics = pollService.getAnswerStatistics(questionId);
        return ResponseEntity.ok(statistics);
    }
}