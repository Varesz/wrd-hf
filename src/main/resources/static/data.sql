-- Create Poll table
CREATE TABLE poll (
    id BIGINT PRIMARY KEY AUTO_INCREMENT
);

-- Create Question table
CREATE TABLE question (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    question VARCHAR(255),
    type VARCHAR(50),
    poll_id BIGINT,
    FOREIGN KEY (poll_id) REFERENCES poll(id)
);

-- Create Answer table
CREATE TABLE answer (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    text VARCHAR(255),
    question_id BIGINT,
    FOREIGN KEY (question_id) REFERENCES question(id)
);

INSERT INTO poll (id) VALUES (1);
INSERT INTO question (id, question, type, poll_id) VALUES
(1, 'Which javascript framework do you like the most?', 'SELECT_ONE', 1),
(2, 'Which IDEs do you like to work with?', 'SELECT_MORE', 1),
(3, 'What''s your proudest moment so far?', 'FREE_TEXT', 1);

INSERT INTO answer (id, text, question_id) VALUES
(1, 'Angular', 1),
(2, 'React', 1),
(3, 'Vue', 1),
(4, 'Svelte', 1);

INSERT INTO answer (id, text, question_id) VALUES
(5, 'Webstorm', 2),
(6, 'IntelliJ IDEA', 2),
(7, 'VSCode', 2),
(8, 'Sublime', 2);